import {Redirect} from "react-router-dom";
import React from "react";
import AssetSwap from './bepswap';
import Swap from './swap/swap';
import Pool from './pool/pool';
import Trade from './trade/trade';

export const routes = [
    {
        path: '/bepswap',
        component: AssetSwap,
        routes: [
            {
                path: '/bepswap',
                exact: true,
                component: () => <Redirect replace to="/bepswap/swap" />
            },
            {
                path: '/bepswap/swap',
                component: Swap,
                tab: 'SWAP'
            },
            {
                path: '/bepswap/pool',
                component: Pool,
                tab: 'POOL'
            },
            {
                path: '/bepswap/trade',
                component: Trade,
                tab: 'TRADE'
            }
        ]
    }
];
