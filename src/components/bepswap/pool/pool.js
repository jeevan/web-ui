import {Component} from "react";
import {withStyles} from "@material-ui/core";
import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid/Grid";
import Avatar from "@material-ui/core/Avatar/Avatar";
import orbGreen from "../../../assets/orbGreen.svg";
import orbBlue from "../../../assets/orbBlue.svg";
import assetArrowGreenDash from "../../../assets/assetArrowGreenDash.svg";
import assetArrowYellowDash from "../../../assets/assetArrowYellowDash.svg";
import assetArrowBlueDash from "../../../assets/assetArrowBlueDash.svg";
import Button from "@material-ui/core/Button/Button";
import assetArrowBlue from "../../../assets/assetArrowBlue.svg";
import {Link} from "react-router-dom";

import styles from './poolStyle';

class Pool extends Component {

    render() {
        const {classes} = this.props;
        return (
            <Grid container spacing={3}>
                <Grid item xs>
                    <Grid
                        container
                        direction="column"
                        justify="flex-start"
                        alignItems="flex-start"
                    >
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1}>
                                <Typography gutterBottom variant="body1" className={classes.poolHeading}>
                                    POOL
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.poolContent}>
                                    You can stake your assets in any of the pools.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.poolContent}>
                                    Each trade on the pool earns a commission which you can later claim.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.poolContent}>
                                    Choose pools with low liquidity and high volume for maximum earnings.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '2em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '2em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '2em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '1.35em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '1.25em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '1.5em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '2em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '2em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}></Grid>
                            <Grid item >
                                <Button className={classes.buttonDisabled} component={Link} to="/bepswap/swap">
                                    <Grid container spacing={2}>
                                        <Grid item xs = {1}/>
                                        <Grid item >
                                            <Typography gutterBottom variant="body1" className={classes.backButtonPool}>
                                                BACK</Typography>
                                        </Grid>
                                    </Grid>
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs>
                    <Grid
                        container
                        direction="column"
                        justify="flex-start"
                        alignItems="flex-start"
                    >
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '1em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '1em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '1em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '1em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '1em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '1em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid container spacing={7}>
                                <Grid item xs style={{ padding: '1em' }}>
                                </Grid>
                                <Grid item style={{ padding: 10 }}>
                                    <Avatar src={assetArrowGreenDash} className={classes.avatarArrowGreenStyle}  />
                                </Grid>
                                <Grid item  >
                                    <Avatar src={orbGreen} className={classes.avatarRoundGreenStyle} />
                                </Grid>
                                <Grid item style={{ padding: 15 }}>
                                    <Avatar src={assetArrowYellowDash} className={classes.avatarYellowArrowStyle} />
                                </Grid>
                                <Grid item xs >
                                </Grid>
                                <Grid item xs >
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid container spacing={7}>
                                <Grid item xs style={{ padding: 20 }}>
                                </Grid>
                                <Grid item style={{ padding: 25 }}>

                                </Grid>
                                <Grid item  >
                                    <Typography gutterBottom variant="body1" className={classes.coinStyleBNB}>
                                        RUNE : BNB
                                    </Typography>
                                </Grid>
                                <Grid item style={{ padding: 15 }}>

                                </Grid>
                                <Grid item xs >
                                </Grid>
                                <Grid item xs >
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid container spacing={7}>
                                <Grid item xs style={{ padding: 20 }}>
                                </Grid>
                                <Grid item style={{ padding: 10 }}>
                                    <Avatar src={assetArrowGreenDash} className={classes.avatarArrowGreenStyle}  />
                                </Grid>
                                <Grid item xs >
                                    <Avatar src={orbBlue} className={classes.avatarRoundGreenStyle} />
                                </Grid>
                                <Grid item style={{ padding: 15 }}>
                                    <Avatar src={assetArrowBlueDash}  className={classes.avatarYellowArrowStyle} />
                                </Grid>
                                <Grid item xs >
                                </Grid>
                                <Grid item xs >
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid container spacing={7}>
                                <Grid item xs style={{ padding: 20 }}>
                                </Grid>
                                <Grid item style={{ padding: 25 }}>

                                </Grid>
                                <Grid item  >
                                    <Typography gutterBottom variant="body1" className={classes.coinStyleBNB}>
                                        RUNE : BEP2
                                    </Typography>
                                </Grid>
                                <Grid item xs >
                                </Grid>
                                <Grid item xs >
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item xs>
                    <Grid
                        container
                        direction="column"
                        justify="flex-start"
                        alignItems="flex-start"
                    >
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '3em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '3em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs style={{ padding: '2.5em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '2.5em' }}>
                            </Grid>
                            <Grid item xs style={{ padding: '3em' }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.poolContent}>
                                    Since anyone can stake alongside you, you own a
                                    share of the pool which adjusts if people join or leave.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1} >
                            </Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.poolContent}>
                                    As trades happen the asset balances will change,
                                    but your share won't.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs={1} wrap="nowrap">
                                <Typography gutterBottom variant="body1" className={classes.poolContent}>
                                    You swap assets by sending them into pools
                                    containing RUNE & BEP2 tokens.
                                </Typography>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={1} style={{ padding: 10 }}>
                            </Grid>
                            <Grid item xs={1}>
                            </Grid>
                            <Grid item xs>
                            </Grid>
                        </Grid>
                        <Grid container spacing={1}>
                            <Grid item xs style={{ padding: 5 }}>
                            </Grid>
                        </Grid>
                        <Grid container spacing={5}>
                            <Grid item xs={3} style={{ padding: 10 }}>
                            </Grid>
                            <Grid item xs={2} >
                            </Grid>
                            <Grid item xs={2} >
                            </Grid>
                            <Grid item xs={2}>
                                <Button className={classes.buttonPool} component={Link} to="/bepswap/trade">
                                    <Grid item>
                                        <Typography gutterBottom variant="body1" className={classes.poolButtonStyle}>
                                            TRADE</Typography>
                                    </Grid>
                                    <Grid item>
                                        <Avatar alt="Blue Arrow" src={assetArrowBlue}
                                                className={classes.poolButtonArrowStyle}/>
                                    </Grid>
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid container spacing={1}>
                            <Grid item xs style={{ padding: 10 }}>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        )
    };
}

export default (withStyles(styles, {withTheme: true})(Pool))
