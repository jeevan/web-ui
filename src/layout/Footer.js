import React from 'react'
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import logoThorchain from './../assets/logoThorchain.svg';

import { withStyles } from '@material-ui/core/styles';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {faFacebookF, faTwitter, faYoutube, faReddit, faGithub, faTelegram} from '@fortawesome/free-brands-svg-icons';

library.add(faFacebookF, faTwitter, faYoutube, faReddit, faGithub, faTelegram);


const styles = theme => ({
  root: {
    boxShadow: 'none',
      left: 0,
      bottom: 0
  },
    toolbar: {
        backgroundColor: '#fff',
        paddingLeft: 32,
        paddingRight: 32,
        [theme.breakpoints.up('md')]: {
            paddingLeft: 126,
            paddingRight: 126,
        },
    },
  grow: {
    flexGrow: 1
  },
  logo: {
    maxHeight: 29.33,
    maxWidth: 154.79,
    [theme.breakpoints.up('md')]: {
      height: 48,
    }
  },
  heading: {
    fontSize: 15,
    width: 380,
    fontWeight: 300,
    letterSpacing: .9,
    color: '#9B9B9B'
  },
  button: {
    borderRadius: 20,
    width: 179,
    backgroundColor: '#ECEEEF',
    borderColor: '#ECEEEF'
  },
  buttonText: {
    borderRadius: 20,
    fontWeight: 600,
    fontSize: 13,
    color: '#9B9B9B',
    letterSpacing: 1.86
  },
  icon: {
    fontSize: 16,
    color: 'green'
  }
});

const Footer = ({classes}) => {

    const thorchainTwitter = "https://twitter.com/thorchain_org?lang=en";
    const thorchainGithub = "https://github.com/thorchain";
    const thorchianTelegram = "https://t.me/thorchain_org";
    const thorchainYoutube = "https://www.youtube.com/channel/UC6ZiZuysJZRFQKv8Zn8OG0g";
    const thorchainReddit = "https://www.reddit.com/r/THORChain/";
    const thorchainOrg = "https://thorchain.org/";

  return (
      <AppBar position="sticky" className={classes.root}>
          <Toolbar className={classes.toolbar} >
              <Grid container spacing={3}>
                  <Grid item xs>
                      <a href={"/"}> <img src={logoThorchain} className={classes.logo}  alt="bepswap logo"/></a>
                  </Grid>
                  <Grid item xs>
                      <a href={thorchainOrg} style={{textDecoration: 'inherit'}}><Typography variant="body1" className={classes.heading} >LEARN MORE</Typography></a>
                  </Grid>
                  <Grid item xs>
                      <Grid container spacing={2} style={{marginTop: 7, marginLeft: 100}}>
                          <Grid item xs={1}>
                              <FontAwesomeIcon icon={faFacebookF} style={{ color: '#919D9D' }}  />
                          </Grid>
                          <Grid item xs={1}>
                              <a href={thorchainTwitter}><FontAwesomeIcon icon={faTwitter} style={{ color: '#919D9D' }}/></a>
                          </Grid>
                          <Grid item xs={1}>
                              <a href={thorchainYoutube}><FontAwesomeIcon icon={faYoutube} style={{ color: '#919D9D' }}/></a>
                          </Grid>
                          <Grid item xs={1}>
                              <a href={thorchainReddit}><FontAwesomeIcon icon={faReddit} style={{ color: '#919D9D' }}/></a>
                          </Grid>
                          <Grid item xs={1}>
                              <a href={thorchainGithub}><FontAwesomeIcon icon={faGithub} style={{ color: '#919D9D' }}/></a>
                          </Grid>
                          <Grid item xs={1}>
                              <a href={thorchianTelegram}><FontAwesomeIcon icon={faTelegram} style={{ color: '#919D9D' }}/></a>
                          </Grid>
                      </Grid>
                  </Grid>
              </Grid>
          </Toolbar>
      </AppBar>
  );
};

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);
