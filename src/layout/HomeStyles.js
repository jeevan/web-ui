const styles = theme => ({
    root: {
        boxShadow: 'none',
        backgroundColor: '#fff',
        borderRadius: 20
    },
    grow: {
        flexGrow: 1
    },
    logo: {
        maxHeight: 19.33,
        maxWidth: 154.79,
        [theme.breakpoints.up('md')]: {
            height: 48,
        }
    },
    heading: {
        fontSize: 15,
        width: 482,
        fontWeight: 300,
        letterSpacing: .9,
        color: '#9B9B9B'
    },
    paperStyle: {
        flexGrow: 1,
        borderRadius: 20,
        paddingBottom: 40
    },
    icon: {
        fontSize: 16,
        color: 'green'
    },
    headerFinishContent: {
        color: '#919D9D',
        fontSize: 20,
        fontWeight: 'bold',
        letterSpacing: 3,
    },
    headerContent: {
        color: '#919D9D',
        fontSize: 20,
        fontWeight: 'bold',
        letterSpacing: 3
    },
    subHeadingContent: {
        color: '#919D9D',
        fontSize: 15,
        fontWeight: 300,
        letterSpacing: 1
    },
    subHeadingContent1: {
        color: '#919D9D',
        fontSize: 15,
        fontWeight: 300,
        letterSpacing: 1
    },
    headerContent2: {
        color: '#919D9D',
        fontSize: 16,
        fontWeight: 'bold',
        letterSpacing: 1
    },
    avatarStyle1: {
        marginTop: '1em',
        borderWidth: 10, borderColor: '#FFFFFF', borderRadius: 50, borderStyle:'solid', boxShadow: '0 0 10px #50E3C2', border: '6px solid #FFF'
    },
    avatarStyle2: {
        marginTop: '1em',
        borderWidth: 10, borderColor: '#FFFFFF', borderRadius: 50, borderStyle:'solid', boxShadow: '0 0 10px #50E3C2', border: '6px solid #FFF'
    },
    button: {
        borderRadius: 64,
        width: 179,
        backgroundColor: '#ECEEEF',
        borderColor: '#ECEEEF',
        shadowOffset: {},
        boxShadow: '1 solid #33CCFF',
        background: 'linear-gradient(#50E3C2 0%, #33CCFF 100%)'
    },
    buttonText: {
        borderRadius: 20,
        fontWeight: 600,
        fontSize: 13,
        color: '#FFFFFF',
        letterSpacing: 1.86
    },
    buttonDisabled: {
        width: 126,
        height: 36,
        backgroundColor: '#FFFFFF',
        border: '1px solid #E8E8E8',
        shadowOffset: {},
        boxSizing: 'border-box'
    },
});

export default styles
