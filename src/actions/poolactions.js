import * as types from './index';

export const setLiquididtyValue = (liquidityValue) => {
    return {
        type: types.LIQUIDITY_VALUE,
        liquidityValue
    }
};

export const setTickerValue = (tickerValue) => {
    return {
        type: types.TICKER_VALUE,
        tickerValue
    }
};

export const setSlipValue = (slipValue) => {
    return {
        type: types.SLIP_VALUE,
        slipValue
    }
};

export const setTickerFinalPriceValue = (tickerFinalPriceValue) => {
    return {
        type: types.TICKER_FINAL_PRICE_VALUE,
        tickerFinalPriceValue
    }
};

export const setRuneFinalPriceValue = (runeFinalPriceValue) => {
    return {
        type: types.RUNE_FINAL_PRICE_VALUE,
        runeFinalPriceValue
    }
};

export const setFinalSlipValue = (x,X) => {
    const sumXX = x + X;
    const slipValue = x*(2*X + x)/(x+X)**2;
    console.log('asdasda asdasdasd asd ' + slipValue)
    return {
        type: types.SLIP_VALUE,
        slipValue
    }
};
