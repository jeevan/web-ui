import { createStore, compose, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk';
import reducers from '../reducers/index'

const initialState = {}

const createStoreWithMiddleware = compose(
  applyMiddleware(
    thunkMiddleware
  )
)(createStore);

const store = createStoreWithMiddleware(reducers, initialState);

export default store
