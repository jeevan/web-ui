import { combineReducers } from 'redux';
import poolreducer from './poolreducer'

const rootReducer = combineReducers({
    poolreducer
});

export default rootReducer;
