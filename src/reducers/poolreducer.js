import * as types from '../actions/index';

export default function(state = [], action) {
    let response = action.response;

    switch (action.type) {
        case types.LIQUIDITY_VALUE:
            return {...state, response};
        case types.TICKER_VALUE:
            return {...state, response};
        case types.RUNE_FINAL_PRICE_VALUE:
            return {...state, response};
        case types.TICKER_FINAL_PRICE_VALUE:
            return {...state, response};
        case types.SLIP_VALUE:
            return {...state, response};
        default:
            return state;
    }
}
