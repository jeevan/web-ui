import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { MuiThemeProvider, createMuiTheme, withStyles } from '@material-ui/core/styles';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import { Header, Home } from './layout';
import Footer from "./layout/Footer";
import logoBepswapLarge from "./assets/logoBepswapLarge.svg";
import {routes} from "./components/bepswap/routes";
import Finish from './layout/Finish';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#33ccff',
      light: '#f0fbff',
    },
    secondary: {
      main: '#f8f8f8',
    }
  }
});

const styles = {
    root: {
        flexGrow: 1,
        paddingLeft: 32,
        paddingRight: 32,
        paddingTop: '3em',
        paddingBottom: '3em',
        [theme.breakpoints.up('md')]: {
            paddingLeft: 126,
            paddingRight: 126,
        },
        backgroundImage: `url(${logoBepswapLarge})`
    }
}

class App extends Component {

    render() {
        const {classes, theme} = this.props;
        return (
            <MuiThemeProvider theme={theme}>
                <Header/>
                <div className={classes.root}>
                    <BrowserRouter>
                        <Switch>
                            <Route exact path="/" component={Home}/>
                            <Route exact path="/finish" component={Finish}/>
                            {RoutesWithSubRoutes(routes)}
                        </Switch>
                    </BrowserRouter>
                </div>
                <Footer/>
            </MuiThemeProvider>
        );
    }
}

const RoutesWithSubRoutes = routes =>
    routes.map((route, i) => (
        <Route
            key={i}
            exact={route.exact || false}
            path={route.path}
            render={props => <route.component {...props} routes={route.routes} />}
        />
    ));

App.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(App);
